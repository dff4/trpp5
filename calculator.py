import sys

def outT(value, inputT, outputT):
    if (inputT == "F"):
        if (outputT == "C"):
            print(round((value - 32) * 5 / 9))
            return (round((value - 32) * 5 / 9))
        if (outputT == "K"):
            print(round((value - 32) * 5 / 9 + 273))
            return (round((value - 32) * 5 / 9 + 273))

    if (inputT == "C"):
        if (outputT == "F"):
            print(round(value * 9 / 5 + 32))
            return (round(value * 9 / 5 + 32))
        if (outputT == "K"):
            print(round(value + 273))
            return (round(value + 273))

    if (inputT == "K"):
        if (outputT == "C"):
            print(round(value - 273))
            return (round(value - 273))
        if (outputT == "F"):
            print(round((value - 273) * 9 / 5 + 32))
            return (round((value - 273) * 9 / 5 + 32))

if len(sys.argv) > 1:
    value = int(sys.argv[1])
    inputT = sys.argv[2]
    outputT = sys.argv[3]
    outT(value, inputT, outputT)